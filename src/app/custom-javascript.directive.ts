import { AfterContentInit, Directive } from '@angular/core';
import { environment } from '../environments/environment';

@Directive({ selector: '[<custom-javascript>]' })
export class CustomJavascriptDirective implements AfterContentInit {
    constructor() {
    }
	
	ngAfterContentInit() {
		if(!environment.production) {
		var s = document.createElement("script");
		s.type = "text/javascript";
		s.src = "https://forio.com/tools/js-libs/1.5.0/epicenter.min.js";
		document.body.appendChild(s);
		}
  }
}