import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from './dependencies/common.service';
import { AuthService } from './dependencies/auth.service';
import { environment } from '../environments/environment';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    constructor(private commonService: CommonService, private authService: AuthService, private router: Router) {

    }

    isToggled(): boolean {
        return this.commonService.isToggled();
    }

    toggle(): void {
        this.commonService.toggle();
    }

    logout() {
        this.authService.logout().subscribe(() => {
            this.authService.isLoggedIn = false;
            this.router.navigate(['/login']);
        }, () => {
        });
    }
}
