﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Login } from './login';
import { AuthService } from '../../dependencies/auth.service';

@Component({
    templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
    error: string;
    model: Login;
    constructor(public authService: AuthService, public router: Router) {
        this.authService.isLoggedIn = false;
    }

    ngOnInit() {
        this.model = new Login();
    }

    login() {
        this.error = '';
        //todo: improve the code quality
        this.authService.login(this.model.userName, this.model.password).subscribe(() => {
            this.authService.isLoggedIn = true;
            // Redirect the user
            this.router.navigate([this.redirectUrl()]);
        }, () => {
            this.error = 'Invalid user name or password';
        });
    }

    private redirectUrl(): string {
        // Get the redirect URL from our auth service
        // If no redirect has been set, use the default
        return this.authService.redirectUrl ? this.authService.redirectUrl : '/home';;
    }
}