import { Injectable, Inject } from '@angular/core';
import { HttpService } from '../../dependencies/http.service';
import { CommonService } from '../../dependencies/common.service';
import { Observable } from 'rxjs/Observable';
import { APP_API_CONFIG, APP_API_DI_CONFIG } from '../../dependencies/app-api-config';

@Injectable()
export class GeneralService {
    constructor(private httpService: HttpService, @Inject(APP_API_CONFIG) private config, private commonService: CommonService) { }

    login(userName: string, password: string) {
        let url = this.commonService.format(this.config.apiEndpoint + this.config.routes.login, userName, password);
        return this.httpService.postData(url, []);
    }

    logout() {
        let url = this.commonService.format(this.config.apiEndpoint + this.config.routes.logout);
        return this.httpService.deleteData(url);
    }

}
