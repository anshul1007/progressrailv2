﻿import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from '../../dependencies/auth-guard.service';

import { OrganizationsComponent } from './organizations.component';
import { OrganizationListComponent } from './organization-list.component';
import { OrganizationDetailComponent } from './organization-detail.component';

const organizationsRoutes: Routes = [
    {
        path: 'organizations',
        component: OrganizationsComponent,
        canActivate: [AuthGuard],
        children: [
            { path: ':id', component: OrganizationDetailComponent },
            { path: '', component: OrganizationListComponent },
        ]
    }];

@NgModule({
    imports: [
        RouterModule.forChild(organizationsRoutes)
    ],
    providers: [AuthGuard],
    exports: [
        RouterModule
    ]
})
export class OrganizationRoutingModule { }