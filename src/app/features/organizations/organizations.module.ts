﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { OrganizationRoutingModule } from './organizations-routing.module';
import { OrganizationsComponent } from './organizations.component';
import { OrganizationListComponent } from './organization-list.component';
import { OrganizationDetailComponent } from './organization-detail.component';

//import { HeroService } from './hero.service';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        OrganizationRoutingModule
    ],
    declarations: [
        OrganizationsComponent,
        OrganizationListComponent,
        OrganizationDetailComponent
    ],
    //providers: [
    //    HeroService
    //]
})
export class OrganizationsModule { }