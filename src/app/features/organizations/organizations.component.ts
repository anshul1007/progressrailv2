﻿import { Component } from '@angular/core';

@Component({
    template: `
    <h1>Main</h1>
    <router-outlet></router-outlet>
  `
})
export class OrganizationsComponent { }