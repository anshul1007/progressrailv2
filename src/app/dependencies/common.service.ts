﻿import { Injectable } from '@angular/core';

@Injectable()
export class CommonService {
    _isToggled = false;
    isToggled(): boolean {
        return this._isToggled;
    }

    toggle(): void {
        this._isToggled = !this._isToggled;
    }

    format(input: string, ...args: any[]): string {
        //let args = Array.prototype.slice.call(arguments, 1);
        return input.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined' ? args[number] : match;
        });
    }

    guid(): string {
        let d = new Date().getTime();
        if (window.performance && typeof window.performance.now === "function") {
            d += performance.now(); //use high-precision timer if available
        }
        var uuid = 'xxxxxxxx-xxxx-xxxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
        return uuid;
    }
}