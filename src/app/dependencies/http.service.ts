import { Injectable } from '@angular/core';
import { Http, Request, RequestOptionsArgs, Response, XHRBackend, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';

@Injectable()
export class HttpService extends Http {
    constructor(backend: XHRBackend, defaultOptions: RequestOptions, private _router: Router) {
        super(backend, defaultOptions);
    }

    getData(url: string): Observable<Response> {
        return this.get(url)
            .map(this.extractData)
            .catch(this.handleError);
    }

    postData(url: string, data: any): Observable<Response> {
        return this.post(url, data)
            .map(this.extractData)
            .catch(this.handleError);
    }

    putData(url: string, data: any): Observable<Response> {
        return this.put(url, data)
            .map(this.extractData)
            .catch(this.handleError);
    }

    deleteData(url: string): Observable<Response> {
        return this.delete(url)
            .map(this.extractData)
            .catch(this.handleError);
    }

    //request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
    //    return this.intercept(super.request(url, options));
    //}

    get(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.intercept(super.get(url, options));
    }

    post(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.intercept(super.post(url, body, this.getRequestOptionArgs(options)));
    }

    put(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.intercept(super.put(url, body, this.getRequestOptionArgs(options)));
    }

    delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.intercept(super.delete(url, options));
    }

    private getRequestOptionArgs(options?: RequestOptionsArgs): RequestOptionsArgs {
        if (options == null) {
            options = new RequestOptions();
        }
        if (options.headers == null) {
            options.headers = new Headers();
        }
        options.headers.append('Content-Type', 'application/json');
        return options;
    }

    private intercept(observable: Observable<Response>): Observable<Response> {
        return observable.catch((err, source) => {
            if (err.status === 401) {
                this._router.navigate(['/login']);
                return Observable.empty();
            } else if (err.status === 403) {
                // show the modal-popup
            } else {
                return Observable.throw(err);
            }
        });
    }

    private extractData(res: Response) {
        let body = res.json();
        return body.data || body || {};
    }

    private handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}
