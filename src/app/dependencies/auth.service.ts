﻿import { Injectable } from '@angular/core';
import { GeneralService } from '../features/general/general.service';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';

@Injectable()
export class AuthService {
    constructor(public generalService: GeneralService) { }
    isLoggedIn: boolean = false;

    // store the URL so we can redirect after logging in
    redirectUrl: string;

    login(userName: string, password: string): Observable<any> {
        return this.generalService.login(userName, password);
        //return Observable.of(true).delay(1000).do(val => this.isLoggedIn = true);
    }

    logout(): Observable<any>  {
        return this.generalService.logout();
    }
}