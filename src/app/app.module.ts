// modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

// directives
import { CustomJavascriptDirective } from './custom-javascript.directive';

// components
import { AppComponent } from './app.component';
import { LoginComponent } from './features/general/login.component';
import { PageNotFoundComponent } from './features/general/page-not-found.component'

// services
import { CommonService } from './dependencies/common.service';
import { HttpService } from './dependencies/http.service';
import { APP_API_CONFIG, APP_API_DI_CONFIG } from './dependencies/app-api-config';
import { AuthService } from './dependencies/auth.service';
import { GeneralService } from './features/general/general.service';

//features
import { AppRoutingModule } from './app-routing.module';
import { OrganizationsModule } from './features/organizations/organizations.module';

@NgModule({
    declarations: [
        AppComponent,
        CustomJavascriptDirective,
        LoginComponent,
        PageNotFoundComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        OrganizationsModule,
        AppRoutingModule
    ],
    providers: [
        CommonService,
        HttpService,
        AuthService,
        GeneralService,
        { provide: APP_API_CONFIG, useValue: APP_API_DI_CONFIG }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
