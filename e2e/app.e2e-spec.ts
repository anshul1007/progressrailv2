import { ProgressrailPage } from './app.po';

describe('progressrail App', function() {
  let page: ProgressrailPage;

  beforeEach(() => {
    page = new ProgressrailPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
